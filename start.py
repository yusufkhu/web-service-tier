from flask import Flask, Response, request, jsonify
from flask_cors import CORS
import requests
import json
from utils.compare import compare
import urllib.parse

def create_app():
    '''Create an app by initializing components'''
    application = Flask(__name__)
    return application


application = create_app()
CORS(application)

@application.route("/")
def hello():
    return "Hello World!"

@application.route('/auth', methods=['POST'])
def auth():
    username = request.get_json()['username']
    password = request.get_json()['password']
    data = {"username": username, "password": password}
    r = requests.post(web_server_config("dao-server")["url"] + "/auth", json=json.dumps(data))

    if r.json()['status'] == 200:
        return jsonify(status=200, msg='OK', role=r.json()['role'])
    else:
        return jsonify(status=403, msg='Not ok')


@application.route('/deals')
def forwardStream():
    # request_args =
    url_encoded = request.args.get('filters')
    url_decoded = urllib.parse.unquote(url_encoded)
    filters = json.loads(url_decoded)
    # filters = json.loads(request.args.get('filters'))
    r = requests.get(web_server_config("data-generate")["url"], stream=True)
    def eventStream():
            for line in r.iter_lines(chunk_size=1):
                if line:
                    line_decoded = json.loads(line.decode())
                    satisfaction = True
                    for filter in filters:
                        if not compare(filter, line_decoded):
                            satisfaction = False
                            break
                    if satisfaction:
                        yield 'data:{}\n\n'.format(line.decode())
    return Response(eventStream(), mimetype="text/event-stream")

@application.route('/getAllCounterparties', methods=['GET'])
def get_cptys():
    r = requests.get(web_server_config("dao-server")["url"] + "/get_cptys")

    if r.json()['status'] == 200:
        return jsonify(status=200, msg='OK', data=r.json()['data'])
    else:
        return jsonify(status=403, msg='Not ok')

@application.route('/getAllInstruments', methods=['GET'])
def get_instruments():
    r = requests.get(web_server_config("dao-server")["url"] + "/get_instruments")

    if r.json()['status'] == 200:
        return jsonify(status=200, msg='OK', data=r.json()['data'])
    else:
        return jsonify(status=403, msg='Not ok')

@application.route('/getAllDealTypes', methods=['GET'])
def get_types():
    r = requests.get(web_server_config("dao-server")["url"] + "/get_types")

    if r.json()['status'] == 200:
        return jsonify(status=200, msg='OK', data=r.json()['data'])
    else:
        return jsonify(status=403, msg='Not ok')

@application.route('/getAverage', methods=['GET'])
def get_avg():
    r = requests.get(web_server_config("dao-server")["url"] + "/avg_B_and_S_for_inst")

    if r.json()['status'] == 200:
        return jsonify(status=200, msg='OK', data=r.json()['data'])
    else:
        return jsonify(status=403, msg='Not ok')



def web_server_config(server):
    with open('./config/server-config.json') as f:
        config = json.load(f)
    return config[server]


if __name__ == '__main__':
    config = web_server_config('web-server')
    print("_______________Running in debug mode_______________")
    application.run(port=config['port'], threaded=True, host=(config['host']))
