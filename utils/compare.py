def compare(filter, line):
    if filter['exp'] == "==":
        if line[filter["field"]] == filter["value"]:
            return True
        else:
            return False
    if filter['exp'] == "!=":
        if line[filter['field']] != filter['value']:
            return True
        else:
            return False
    if filter['exp'] == "<=":
        if line[filter['field']] <= filter['value']:
            return True
        else:
            return False
    if filter['exp'] == ">=":
        if line[filter['field']] >= filter['value']:
            return True
        else:
            return False
    if filter['exp'] == "<":
        if line[filter['field']] < filter['value']:
            return True
        else:
            return False
    if filter['exp'] == ">":
        if line[filter['field']] > filter['value']:
            return True
        else:
            return False
